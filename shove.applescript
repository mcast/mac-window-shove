
to findDesktops()
	tell application "Finder"
		set deskSize to bounds of window of desktop
		
		-- scr1 is primary (has the menubar)
		set scr1 to {0, 0, 1920, 1200} -- LA2405 monitor
		
		-- scr2 is offset right & down
		set scr2sz to {1280, 800} -- laptop panel
		set scr2 to {(item 3 of deskSize) - (item 1 of scr2sz), (item 4 of deskSize) - (item 2 of scr2sz), �
			(item 1 of scr2sz), (item 2 of scr2sz)}
	end tell
	return {scr1, scr2, deskSize}
end findDesktops

to shoveWindow of procName given deskNum:deskNum, spacing:gapFrac, deskState:deskState, raise:raise
	set scr to item deskNum of deskState
	tell application "System Events"
		tell process procName
			set w to window 1
			set oldPos to position of w
			set sz to size of w
			set gap to {�
				(item 3 of scr) - (item 1 of sz), �
				(item 4 of scr) - (item 2 of sz)}
			set newPos to {�
				(item 1 of scr) + (item 1 of gap) * (item 1 of gapFrac), �
				(item 2 of scr) + (item 2 of gap) * (item 2 of gapFrac)}
			set position of w to newPos
			if (position of w) � newPos then
				-- repeat? sometimes it leaves x-=40 offset
				-- due to screen non-rectangularity?
				set position of w to newPos
			end if
			if raise then
				set frontmost to true
			end if
		end tell
	end tell
end shoveWindow

set d to findDesktops()
tell application "System Events"
	if (name of current desktop) = "LA2405" then
		get name of current location of network preferences
	end if
end tell
shoveWindow of "VLC" without raise given deskState:d, deskNum:2, spacing:{0, 1}
shoveWindow of "Mail" with raise given deskState:d, deskNum:2, spacing:{1, 0.5}
shoveWindow of "Calendar" with raise given deskState:d, deskNum:2, spacing:{0, 0}

-- With help from the web, including
--   http://stackoverflow.com/questions/13173590/is-it-possible-to-move-windows-in-background-without-stealing-focus
--   http://stackoverflow.com/a/11488245
--   https://developer.apple.com/library/mac/documentation/AppleScript/Conceptual/AppleScriptLangGuide/introduction/ASLR_intro.html
